+++
title = "KiCad 6.0.0 Release"
date = "2021-12-25"
draft = false
aliases = [
    "/post/release-6.0.0/"
]

"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the release of version 6.0.0.
This is the first major version release of KiCad since 5.0.0 was released in
July of 2018.

KiCad binaries are available for download for Windows, MacOS, and
Linux or will be in the very near future.  See the
https://www.kicad.org/download/[KiCad download page] for guidance.

There have been many important changes that make this release a substantial
improvement over the 5.x series and a worthwhile upgrade for users on all
platforms.  There are hundreds of new features and improvements, as well as
hundreds of bugs that have been fixed.  Below are some of the highlights of the
new release.

Thank you to all the developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.
We hope you enjoy the latest release of KiCad.

-- The KiCad Development Team

== Modern, consistent look and feel

KiCad 6.0 features a refreshed user interface designed to reduce the barriers
of entry for new users and ease friction when switching between KiCad and other
design software.  The visual design language, hotkeys, dialog layouts, and
editing workflows have been harmonized across KiCad so that it no longer feels
like you are using two different tools when switching between the schematic and
PCB editors.

{{< figure
    src="schematic_editor.png"
    link="schematic_editor.png"
    target=blank
    width=90%
    alt="Schematic editor"
    title="Jeff Young, Seth Hillbrand, Ian McInerney, Jon Evans, et. al."
>}}

== Upgraded schematic editing

KiCad's schematic editor has received its biggest overhaul ever for version
6.0.  It now uses the same object selection and manipulation paradigm as the
PCB editor, and has received dozens of new features to empower your design.
Assign net classes from within the schematic editor, and apply line color and
style rules to wires and buses individually or based on net class.  Create
buses that group together multiple signals with different names for easier
hierarchical design.

KiCad 6.0 also features a brand-new schematic and symbol library file format,
based on the format used for KiCad board and footprint files.  This new format
enables long-desired features such as embedding symbols used in a schematic
directly in the schematic file, so that cache libraries are no longer needed.

{{< figure
    src="schematic_line_styles.png"
    link="schematic_line_styles.png"
    target=blank
    width=90%
    alt="Schematic editor line styles"
    title="Jeff Young, Wayne Stambaugh, Jon Evans, et. al."
>}}

== Improved PCB design experience

KiCad's PCB editor has had a complete look and feel upgrade, featuring many new
options to help you navigate complicated designs.  Save and restore custom view
presets.  Hide certain nets from the ratsnest.  Control the opacity of zones,
pads, vias, and tracks independently.  Set colors for specific nets and net
classes, and apply those colors to the ratsnest or to all the copper attached
to that net.  Use the new Selection Filter panel in the lower-right corner of
the editor to control what types of objects can be selected.

{{< figure
    src="pcb_editor.png"
    link="pcb_editor.png"
    target=blank
    width=90%
    alt="PCB Editor UI"
    title="Jon Evans, Jeff Young"
>}}

Create advanced boards with KiCad's added support for rounded tracks, hatched
copper zones, and unconnected annular ring removal.  The push-and-shove router
and track length tuning features have also been improved for faster, easier,
and more user-friendly routing.

{{< figure
    src="pcb_hatched_and_rounded.png"
    link="pcb_hatched_and_rounded.png"
    target=blank
    width=90%
    alt="Hatched zone fill and rounded tracks"
    title="Seth Hillbrand, Jon Evans, Roberto Fernandez Bautista, Jeff Young, Jean-Pierre Charras, Tomasz Wlostowski, et. al."
>}}

Visualize your board in more ways than ever with KiCad's updated 3D viewer,
featuring raytracing lighting controls, highlighting of objects selected in the
PCB editor, and easier access to frequently-used controls.

{{< figure
    src="raytracer.png"
    link="raytracer.png"
    target=blank
    width=90%
    alt="Raytraced 3D rendering"
    title="Mario Luzeiro"
>}}

A new custom design rule system allows complex design rules to be defined,
including area-specific rules, layer-specific rules, and other constraints
required for advanced designs.

{{< figure
    src="drc_rules.png"
    link="drc_rules.png"
    target=blank
    width=90%
    alt="Custom design rules"
    title="Jeff Young, Tomasz Wlostowski"
>}}

== And much more...

With the thousands of changes made between KiCad 5 and KiCad 6, it is difficult
to capture all the new and improved features in one place.  Below is a partial
list of new features in KiCad 6.  In addition to these new features, hundreds
of bug fixes and performance improvements have been contributed by dozens of
volunteers.  Thank you to everyone who helped make KiCad 6 our biggest release
ever.

=== Project management and core functionality

- Integrated automatic project backup system (Jon Evans)
- Plugin and Content Manager (Andrew Lutsenko)
- Side-by-side installation with independent settings (Jon Evans)
- Expanded mouse and touchpad settings (Jon Evans)
- Dark mode support on Linux and macOS (Jon Evans)
- Support for displaying measurements in inches (Mikołaj Wielgus)
- Project-wide text replacement variables (Jeff Young)
- Standardized recommended user content folder locations (Mark Roszko) 

=== Schematic and symbol editing

- Intersheet references (Franck Jullien)
- One-click wire start (Mark Roszko)
- Align to Grid action (Seth Hillbrand)
- Edit Text and Graphic Properties dialog (Jeff Young)
- Mark certain ERC errors as ignored (Jeff Young)
- Additional ERC checks (Jon Evans)
- Assign net classes inside schematic (Jeff Young)
- Line color and style by net class (Jeff Young)
- More flexible annotation of pasted objects (Roberto Fernandez Bautista)
- Improved symbol inheritance model (Wayne Stambaugh)
- Improved page number support (Wayne Stambaugh)

=== PCB and footprint editing

- Drag components with traces attached (Tomasz Wlostowski)
- Push-and-shove router improvements (Tomasz Wlostowski, Jon Evans)
- Remove unused annular rings from vias and pads (Seth Hillbrand)
- 3D viewer performance and visual improvements (Mario Luzeiro, Oleg Endo)
- Drilled holes statistics table (Mikołaj Wielgus)
- New dimension types (Jon Evans)
- Convert between graphic lines, polygons, and tracks (Jon Evans)
- Multi-layer copper zones and rule areas (Jon Evans)
- Graphical board stackup table (Fabien Corona)
- Improved snapping behavior (Seth Hillbrand)
- Improved net inspector tool (Oleg Endo)
- Object grouping (Joshua Redstone)
- Geographic Re-annotation (Brian Piccioni)
- Physical stackup editor (Jean-Pierre Charras)
- Easy what-you-see-is-what-you-get custom footprint pad creation (Jeff Young)
- Allow inverting coordinate axes (Reece Pollack)

=== Simulation and utilities

- Expanded support for SPICE simulation (Sylwester Kocjan)
- Improvements to simulation GUI (Mikołaj Wielgus)
- E-Series resistor calculator (janvi)
- Improvements to GerbView "Export to PCB" (Peter Montgomery)

=== Plotting, importing and exporting

- Altium Designer importer (Thomas Pointhuber)
- CADSTAR importer (Roberto Fernandez Bautista)
- EAGLE importer improvements (Jeff Young, Seth Hillbrand, Jan Mrázek, Thomas
  Pointhuber)
- Improved Gerber format support (Jean-Pierre Charras)
- Selectable color themes for printing (Jon Evans)
- Improved STEP format support (Mark Roszko, Seth Hillbrand)
- Improved DXF import support (Mark Roszko, Tomasz Wlostowski)
- HyperLynx exporter (Tomasz Wlostowski)

=== Deprecation notes

The following features are deprecated as of version 6.0:

- XSLT BOM generation scripts will no longer be supported in the next major
  release of KiCad, and KiCad 6.0 no longer ships with example XSLT scripts.
  If you currently use XSLT scripts, please migrate to Python BOM scripts
  during the 6.x release cycle.
- The SWIG Python API will be removed in a future release of KiCad and replaced
  with a new Python API.  Users may continue to use the SWIG API and report
  bugs against it during the 6.x release cycle, but no new major features will
  be added to the SWIG API.

=== Obtaining KiCad

Version 6.0.0 is built from the source code in the
link:https://gitlab.com/kicad/code/kicad/-/tree/6.0[GitLab 6.0]
branch.
