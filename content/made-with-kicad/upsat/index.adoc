+++
title = "UPSat"
projectdeveloper = "Libre Space Foundation"
projecturl = "https://upsat.gr/"
"made-with-kicad/categories" = [ "Space" ]
+++

The first open source satellite

UPSat is a 2U Cubesat satellite constructed and delivered by
Libre Space Foundation, started by University of Patras as part of the
upsatQB50 mission with ID GR-02.
